


class SwitchDemo{

	public static void main(String[] args){
	
		String data = "Kanha";


		System.out.println("Before Switch");

		switch(data){
		
			case "Ashish":
				
				System.out.println("Barclays");
				break;

			case "Kanha": 

				System.out.println("BMC Software");
				break;

			case "Rahul":

				System.out.println("Infosys");
				break;

			case "Badhe" :

				System.out.println("IBM");
				break;

			default:
				System.out.println("In Default");
		}

		System.out.println("After switch");
	}
}
