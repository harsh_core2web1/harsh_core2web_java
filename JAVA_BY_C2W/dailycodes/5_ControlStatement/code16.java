


class SwitchDemo{

	public static void main(String[] args){
	
		char data = 'B';


		System.out.println("Before Switch");

		switch(data){
		
			case 'A':
				
				System.out.println("a");
				break;
			case 'B': 
				System.out.println("b");
				break;

			case 'C':

				System.out.println("c");
				break;

			default:
				System.out.println("In Default");
		}
	}
}
