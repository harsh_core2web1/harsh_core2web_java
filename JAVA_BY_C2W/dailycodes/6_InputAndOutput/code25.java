

import java.io.*; 

class InputDemo{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 

		System.out.print("Enter company name: ");

		String cmpName = br.readLine();

		System.out.print("enter employee name: ");

		String name = br.readLine(); 

		System.out.print("Enter emplID: ");

		int empID = Integer.parseInt(br.readLine()); 

		System.out.println("company name: "+cmpName);
		System.out.println("Employee name: "+ name);
		System.out.println("Employee ID: "+ empID);
	}
}
