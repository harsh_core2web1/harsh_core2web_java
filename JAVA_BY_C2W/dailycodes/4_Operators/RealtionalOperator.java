


class Relational{

	public static void main(String[] args){
	
		int x = 8;
		int y = 8;                          // Relational always return in boolean values


		System.out.println(x == y); // True
		System.out.println(x != y); // false
		System.out.println(x < y);  // false
		System.out.println(x > y);  // false
		System.out.println(x >= y); // true
		System.out.println(x <= y); // true
	}
}
