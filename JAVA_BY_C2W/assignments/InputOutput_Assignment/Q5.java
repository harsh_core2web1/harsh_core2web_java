
import java.io.*; 

class InputDemo{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 

		System.out.print("Enter number: ");

		int num = Integer.parseInt(br.readLine()); 

		if(num % 16 == 0){
		
			System.out.println(num+ "is present in the table of 16");
		}
	}
}
