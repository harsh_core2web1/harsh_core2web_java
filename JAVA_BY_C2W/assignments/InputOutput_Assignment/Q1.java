

import java.io.*; 

class InputDemo{

	public static void main(String[] arsg)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 

		System.out.print("Enter Value: ");
	       	
		int num = Integer.parseInt(br.readLine()); 

		if(num %2 == 0){
		
			System.out.println(num+"is an even number");
		}else{
		
			System.out.println(num+"is an odd number");
		}
	}
}
