
import java.io.*; 

class InputDemo{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 

		System.out.print("Enter age: ");

		int age = Integer.parseInt(br.readLine()); 

		if(age >= 18){
		
			System.out.println("Voter is Eligible For Voting");
		}else if( age >1 && age <18){
		
			System.out.println("Voter is Not Eligible for Voting");
		}else{
		
			System.out.println("Invalid age");
		}
	}
}
