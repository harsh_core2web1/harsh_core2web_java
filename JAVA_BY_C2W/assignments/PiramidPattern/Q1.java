
import java.io.*; 

class Piramid{

	public static void main(String[] arsg)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 

		System.out.print("Enter ROW: "); 

		int row = Integer.parseInt(br.readLine()); 

		for(int i=1; i<=row; i++){
		
			for(int sp = 0; sp<= row-i; sp++){
			
				System.out.print("\t");
			}
			int num =1;

			for(int j=1; j<= i*2-1; j++ ){
			
				System.out.print(num+"\t");
			}

			System.out.println(); 
		}
	}
}
