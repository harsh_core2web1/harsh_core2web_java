
import java.io.*; 

class Piramid{

	public static void main(String[] arsg)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 

		System.out.print("Enter ROW: "); 

		int row = Integer.parseInt(br.readLine()); 


		 

		for(int i=1; i<=row; i++){

			 char ch1 = 'A'; 
			 char ch2 = 'a'; 
		
			for(int sp = 0; sp<= row-i; sp++){
			
				System.out.print("\t");
			}
			

			for(int j=1; j<= i*2-1; j++ ){
			
				if(j<i){
					if(i%2 == 1){
				
						System.out.print( ch1++ +"\t");
						
					}else{
				
						System.out.print(ch2++  +"\t");
					}
				}else{
				
					if(i%2 == 1){
					
						System.out.print(ch1-- +"\t");
					}else{

						System.out.print(ch2-- + "\t");
					}
				}
				
			}

			System.out.println(); 
		}
	}
}
