
import java.io.*; 

class OneDArray{

	public static void main(String[] arsg)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 

		System.out.println("ENter size");
		int size = Integer.parseInt(br.readLine());  

		int arr[] = new int[size]; 

		System.out.println("Enter Array Element: ");
		for(int i=0; i<size; i++){
		
			arr[i] = Integer.parseInt(br.readLine()); 
		}

		System.out.println("Output: ");

		for(int i=0; i<size; i++){
		
			for(int j=arr[i]-1; j>0; j--){
			
				arr[i] = arr[i]*j; 
			}
		}


		for(int i=0; i<size; i++){
		
			System.out.println(arr[i]);
		}
	}
}
