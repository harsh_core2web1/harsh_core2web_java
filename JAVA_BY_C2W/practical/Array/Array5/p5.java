
import java.util.Scanner; 

class OneDArray{

	public static void main(String[] arsg){
	
		Scanner sc = new Scanner(System.in); 

		System.out.println("ENter size");
		int size = sc.nextInt(); 

		int arr[] = new int[size]; 

		System.out.println("Enter Array Element: ");
		for(int i=0; i<size; i++){
		
			arr[i] = sc.nextInt(); 
		}

		System.out.println("Output: ");


		for(int i=0; i<size; i++){
		
			int temp = arr[i]; 
			int count = 0; 

			while(temp >0){
			
				int rem = temp%10; 
				count++; 
				temp/=10; 
			}
			
			System.out.println(count);
		}
	}
}
