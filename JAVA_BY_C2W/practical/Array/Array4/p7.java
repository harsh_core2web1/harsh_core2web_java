
import java.io.*; 

class OneDArray{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 
		System.out.println("Enter array size: ");
		int size = Integer.parseInt(br.readLine());

		char arr[] = new char[size]; 
		System.out.println("Enter array element: ");

		for(int i=0; i< size; i++){
		
			arr[i] = (char)(br.read());
		       		br.skip(1);  	
		}

		for(int i=0; i<size; i++){

			if(arr[i]>=97 && arr[i]<=122){
			
				arr[i] = (char)(arr[i]-32); 
				 
			}
		
			
		}

		System.out.println("Output: "); 
		
	       	for(int i=0; i<size; i++){
		
			System.out.println(arr[i]);
		}	
	}
}
