

import java.io.*; 

class OneDArray{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 

		System.out.print("Enter Array Size: ");
		int size = Integer.parseInt(br.readLine()); 

		int arr[] = new int[size];

		System.out.println("Enter Array Element: ");
		for(int i=0; i<size; i++){
		
			arr[i] = Integer.parseInt(br.readLine()); 
		}

		int count = 0; 
		int Avg = 0; 

		for(int i=0; i< size; i++){
		
			count = count+arr[i]; 
		}

		Avg = count/size; 

		System.out.println("Array Element Avg is : "+Avg);
	}
}
