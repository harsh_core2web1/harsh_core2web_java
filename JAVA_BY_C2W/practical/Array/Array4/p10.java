

import java.io.*; 

class OneDArray{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 
		
		System.out.println("array size:"); 
		int size = Integer.parseInt(br.readLine()); 

		char arr[] = new char[size]; 

		System.out.println("Enter Array Element: ");
		for(int i=0; i<arr.length; i++){
		
			arr[i] = (char)(br.read());
		       		br.skip(1); 	
		}
		
		System.out.print("Enter character Key: ");
		char temp = (char)(br.read()); 

		System.out.println("Output: ");
		for(int i=0; i<size; i++){
		
			if(arr[i]== temp){
			
				break; 
			}else{
			
				System.out.println(arr[i]);
			}
		}

	}
}
