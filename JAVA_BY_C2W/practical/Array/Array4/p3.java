

import java.io.*; 

class OneDArray{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 

		System.out.print("Enter Array Size: ");
		int size = Integer.parseInt(br.readLine()); 

		int arr[] = new int[size];

		System.out.println("Enter Array Element: ");
		for(int i=0; i<size; i++){
		
			arr[i] = Integer.parseInt(br.readLine()); 
		}

		int max = arr[0]; 

		for(int i=0; i<size; i++){
		
			if(arr[i] > max){
			
				max = arr[i]; 
			}
		}

		int SecondMax = arr[0]; 
		for(int i=0; i<size; i++){
		
			if(arr[i] >= SecondMax && arr[i] < max ){
			
				SecondMax = arr[i]; 
			}
		}

		System.out.println("The Second Largest Element is :" +SecondMax);
	}
}
