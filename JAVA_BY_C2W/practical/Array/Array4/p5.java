
import java.io.*; 

class OneDArray{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 

		System.out.println("Enter array Size :");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size]; 

		System.out.println("Enter array Elemnt :");
		for(int i=0; i<size; i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		for(int i=0; i<size/2; i++){
		
			int temp = arr[i];
			arr[i] = arr[size-1-i]; 
			arr[size-1-i] = temp; 
		}


		System.out.println("Reversed Array : ");

		for(int i=0; i<size; i++){
		
			System.out.println(arr[i]);
		}


	}
}
