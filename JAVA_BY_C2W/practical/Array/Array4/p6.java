

import java.util.Scanner; 

class OneDArray{

	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in); 	

		System.out.println("Enter array size :");
		int size = sc.nextInt(); 

		char arr[] = new char[size]; 


		System.out.println("Enter array element: ");
		for(int i=0; i<size; i++){
		
			arr[i] = sc.next(); 
		}

		int count1 = 0; 
		int count2 = 0; 

		for(int i=0; i<size; i++){
		
			if(arr[i]== 'A' || arr[i]=='E' ||arr[i]=='I'||arr[i]=='O'||arr[i]=='U'){
			
				count1++; 
			}else if(arr[i]== 'a' || arr[i]=='e' ||arr[i]=='i'||arr[i]=='o'||arr[i]=='u'){

				count1++; 
				
			}else{
			
				count2++; 
			}
		}

		System.out.print("Count Of Vowels: "+count1);
		System.out.print("Count Of Consonants:"+count2);
	}	

}
