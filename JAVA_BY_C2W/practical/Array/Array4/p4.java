

import java.util.Scanner; 

class OneDArray{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in); 

		System.out.print("Enter array Size: " ); 
		int size = sc.nextInt(); 

		int arr[] = new  int[size];

		System.out.println("Enter array Element: ");
		
		for(int i=0; i< size; i++){
		
			arr[i] = sc.nextInt(); 
		}	

		System.out.println("Enter array Element to check: ");
		int temp = sc.nextInt(); 

		int count = 0; 
		for(int i=0; i<size; i++){
		
			if(arr[i] == temp){
			
				count++; 

			}
		}
		
		System.out.println(temp+ " occurs more than "+count+" times in the array");

	}
}
