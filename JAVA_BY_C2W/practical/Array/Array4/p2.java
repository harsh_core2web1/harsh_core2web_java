

import java.io.*; 

class OneDArray{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 

		System.out.print("Enter Array Size: ");
		int size = Integer.parseInt(br.readLine()); 

		int arr[] = new int[size];

		System.out.println("Enter Array Element: ");
		for(int i=0; i<size; i++){
		
			arr[i] = Integer.parseInt(br.readLine()); 
		}

		int max = arr[0]; 
		for(int i=0; i<size; i++){
		
			if(arr[i] > max){

				max = arr[i]; 
			}
		}

		int min = arr[0]; 
		for(int i=0; i<size; i++){
		
			if(arr[i] < min){
			
				min = arr[i]; 
			}
		}


		int diff = max-min;

		System.out.println("The difference between the minimum and maximum element is :" +diff);	

		
	}
}
