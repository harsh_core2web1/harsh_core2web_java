

class WhileLoop{

	public static void main(String[] args){
	
		int num = 214367689;
		int oddcount = 0;
		int evencount = 0; 

		while(num >0){
		
			int rem = num%10;
			if(rem % 2== 0){
				
				evencount++; 
			} else{
			
				oddcount++; 
			}
			num/=10; 
		}

		System.out.println("Odd count: "+oddcount);
	
		System.out.println("Even count: "+evencount);

	}
}
