

class pro10{

	public static void main(String[] args){
	
		int num = 9367924; 
		int sum = 0;
		int product = 1;

		while(num>0){
		
			int rem = num%10;
			if(rem%2 == 0){
			
				sum = sum+rem; 
			}else{
			
				product = product*rem;
			}
			num = num/10; 

		}

		System.out.println("Sum of even digits: "+sum);
		System.out.println("Product of odd digits: "+product);
	}
}
