class Prog3{
	public static void main(String[] args){
		for(int num = 1; num <= 9; num++){
			if(num % 2 == 0){
				System.out.print(num * num + " ");
			}
			else{
				System.out.print(num + " ");
			}
		}
	}
}
// 1 4 3 16 5 36 7 64 9
